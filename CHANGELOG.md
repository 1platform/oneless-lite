# Oneless Lite Changelog

Here we will note all the changes made to the Oneless Lite Serverless Framework.

## Version 0.1.0

Initial application release.

Strip the CLI functionalities and TypeScript elements from the production build.
