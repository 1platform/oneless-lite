import HTTPStatus from "../utils/HTTPStatus";

export default interface IOneMessageResponse {
  code: HTTPStatus;
  status: "ok";
  message: string;
}
