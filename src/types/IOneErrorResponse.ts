import HTTPStatus from "../utils/HTTPStatus";

export default interface IOneErrorResponse {
  code: HTTPStatus;
  status: "error";
  message: string;
}
