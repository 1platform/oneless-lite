import type { Environment } from "../utils/Env";
import type { Logger } from "../utils/Logger";

export default interface IOneContext {
  readonly env: Environment;
  readonly log: Logger;

  get(field: string, defaultValue?: string): string | null;

  int(field: string, defaultValue?: number): number;

  number(field: string, defaultValue?: number): number;

  boolean(field: string): boolean;

  array(field: string, separator?: string, defaultValue?: string): string[];

  string(field: string, defaultValue?: string): string;

  flag(flagName: string): boolean;

  url(field: string, defaultValue?: string): string;
}
