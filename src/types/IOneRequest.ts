import { ParsedQs } from "qs";

export default interface IOneRequest<BodyRequest = any> {
  /**
   * Getter for the list of URL Parameters exposed by the endpoint.
   */
  readonly parameters: Record<string, string>;
  /**
   * Getter for the list of Query Parameters exposed by the endpoint.
   */
  readonly queryParameters: ParsedQs;
  /**
   * Getter for the list of Headers exposed by the endpoint.
   */
  readonly headers: Record<string, string>;
  /**
   * Getter for the Body of the request. If the request is a GET one, then it will return undefined.
   */
  readonly body: BodyRequest;

  /**
   * Method used to get the value of a URL Parameter by its name.
   *
   * @param name The name of the parameter.
   */
  getParameter(name: string): string;

  /**
   * Method used to get a numeric URL Parameter by its name.
   *
   * @param name The name of the parameter.
   */
  getParameterNumber(name: string): number;

  /**
   * Method used to get the value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  getQuery(name: string, defaultValue): ParsedQs;

  /**
   * Method used to get the string value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  getQueryString(name: string, defaultValue): string;

  /**
   * Method used to get the numeric value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  getQueryNumber(name: string, defaultValue): number;

  /**
   * Method used to get the boolean value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   */
  getQueryBoolean(name: string): boolean;

  /**
   * Method used to get the array value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   */
  getQueryArray<T = ParsedQs>(name: string): Array<T>;

  /**
   * Method used to get the header value from the request.
   *
   * @param headerName The name of the header value needed.
   */
  getHeader(headerName: string): string | undefined;

  /**
   * Method used to get the contents of the body as the given type passed when defining the endpoint.
   */
  getBody(): BodyRequest;

  /**
   * Method used to get a property from the body using a name and a specific type.
   *
   * @param property The property we want from the body.
   */
  getBodyProperty<T = any>(property: string): T;
}
