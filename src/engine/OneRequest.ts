import { Request } from "express";
import { ParsedQs } from "qs";
import IOneRequest from "../types/IOneRequest";

/**
 *
 */
export default class OneRequest<BodyRequest = any> implements IOneRequest<BodyRequest> {
  /**
   * The request object for which you need context.
   */
  private readonly _request: Request;

  /**
   * OneRequest constructor.
   *
   * @param request The request object for which you need context.
   */
  public constructor(request: Request) {
    this._request = request;
  }

  /**
   * Getter for the list of URL Parameters exposed by the endpoint.
   */
  public get parameters(): Record<string, string> {
    return this._request.params;
  }

  /**
   * Getter for the list of Query Parameters exposed by the endpoint.
   */
  public get queryParameters(): ParsedQs {
    return this._request.query;
  }

  /**
   * Getter for the list of Headers exposed by the endpoint.
   */
  public get headers(): Record<string, string> {
    return this._request.headers as Record<string, string>;
  }

  /**
   * Getter for the Body of the request. If the request is a GET one, then it will return undefined.
   */
  public get body(): BodyRequest {
    return this._request.body;
  }

  /**
   * Method used to get the value of a URL Parameter by its name.
   *
   * @param name The name of the parameter.
   */
  public getParameter(name: string): string {
    return this._request.params[name];
  }

  /**
   * Method used to get a numeric URL Parameter by its name.
   *
   * @param name The name of the parameter.
   */
  public getParameterNumber(name: string): number {
    return Number(this.getParameter(name));
  }

  /**
   * Method used to get the value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  public getQuery(name: string, defaultValue = ""): ParsedQs {
    return (this._request.query[name] || defaultValue) as ParsedQs;
  }

  /**
   * Method used to get the string value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  public getQueryString(name: string, defaultValue = ""): string {
    return this._request.query[name].toString() || defaultValue;
  }

  /**
   * Method used to get the numeric value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   * @param defaultValue The default value of the parameter.
   */
  public getQueryNumber(name: string, defaultValue = 0): number {
    return Number(this.getQueryString(name)) || defaultValue;
  }

  /**
   * Method used to get the boolean value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   */
  public getQueryBoolean(name: string): boolean {
    let value = this.getQueryString(name) ?? "false";

    if (value === "0" || value === "1") {
      value = (value === "1").toString();
    }
    return value.toLowerCase() !== "false";
  }

  /**
   * Method used to get the array value of a Query Parameter by its name.
   *
   * @param name The name of the parameter.
   */
  public getQueryArray<T = ParsedQs>(name: string): Array<T> {
    return (this.getQueryString(name) as unknown as Array<T>) || [];
  }

  /**
   * Method used to get the header value from the request.
   *
   * @param headerName The name of the header value needed.
   */
  public getHeader(headerName: string): string | undefined {
    return this._request.header(headerName) || undefined;
  }

  /**
   * Method used to get the contents of the body as the given type passed when defining the endpoint.
   */
  public getBody(): BodyRequest {
    return this._request.body as BodyRequest;
  }

  /**
   * Method used to get a property from the body using a name and a specific type.
   *
   * @param property The property we want from the body.
   */
  public getBodyProperty<T = any>(property: string): T {
    return this._request.body[property] as T;
  }
}
