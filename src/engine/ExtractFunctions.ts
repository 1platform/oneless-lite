import { existsSync, readdirSync, readFileSync, statSync } from "fs";
import { basename, delimiter, extname, join, resolve } from "path";
import type IFunction from "../types/IFunction";
import { ExtractorHook } from "../types/IFunction";
import HTTPError from "./HTTPError";
import AppLogger from "../utils/Logger";

const functions: Record<string, IFunction> = {};

export default async function ExtractFunctions(baseFolder: string, hooks?: Array<ExtractorHook>): Promise<Record<string, IFunction>> {
  const packageJson = JSON.parse(readFileSync(resolve(baseFolder, "package.json"), "utf-8"));
  const onelessDefinition = {
    functions: "functions",
    searchPath: [ "src", "dist", "." ],
    path: [],
    ...(packageJson.oneless ?? {}),
  };

  let functionsFolderPath = "";
  for (const searchPath of onelessDefinition.searchPath) {
    const functionsPath = resolve(baseFolder, searchPath, onelessDefinition.functions);
    if (existsSync(functionsPath)) {
      functionsFolderPath = functionsPath;
      break;
    }
  }

  if (onelessDefinition.path && onelessDefinition.path.length > 0) {
    const newPath = onelessDefinition.path.reduce((accum, customPath) => {
      if (!customPath.startsWith(".")) {
        return [ ...accum, resolve(customPath) ];
      }

      return [ ...accum, ...onelessDefinition.searchPath.map((searchPath) => resolve(baseFolder, searchPath, customPath)), resolve(customPath) ];
    }, []);
    newPath.push(...process.env.PATH.split(delimiter));
    process.env.PATH = newPath.join(delimiter);
  }

  if (!functionsFolderPath) {
    throw new HTTPError("Unable to find a folder for your functions.");
  }

  const functionsFolder = functionsFolderPath;
  const files = readdirSync(functionsFolder);

  const functionFiles = files
    .filter((file) => [ ".ts", ".js", ".tsx", ".jsx" ].indexOf(extname(file)) >= 0)
    .map((file) => join(functionsFolder, file))
    .filter((file) => statSync(file).isFile());

  for await (const functionFile of functionFiles) {
    try {
      let FunctionModule = await import(functionFile);
      FunctionModule = FunctionModule.default || FunctionModule;
      if (!FunctionModule && typeof FunctionModule !== "function") {
        continue;
      }

      let functionName = basename(functionFile, extname(functionFile));
      let functionExtension = extname(functionName);
      let method = "post";

      if (functionExtension) {
        functionExtension = functionExtension.substring(1);
        method = [ "post", "get", "put", "patch", "delete" ].includes(functionExtension.toLowerCase()) ? functionExtension.toLowerCase() : "post";
        functionName = basename(functionName, `.${ functionExtension }`);
      }

      functions[functionFile] = {
        method: method.toLowerCase(),
        name: functionName,
        file: functionFile,
        function: FunctionModule,
      };
    } catch (err) {
      AppLogger.error(err.message);
      AppLogger.silly(err.stack);
    }
  }

  if (hooks && hooks.length > 0) {
    const resolvedFolder = resolve(baseFolder);
    hooks.forEach((hook) => {
      hook(functions, {
        basePath: resolvedFolder,
        functionsFolder: functionsFolder,
      });
    });
  }

  return functions;
}
