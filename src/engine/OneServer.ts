import type { Application, NextFunction, Request, Response } from "express";
import express from "express";
import { Server } from "http";
import helmet from "helmet";
import bodyParser from "body-parser";
import cors from "cors";
import morgan from "morgan";
import { performance, PerformanceObserver } from "perf_hooks";
import { getVersion } from "../version";
import AppLogger from "../utils/Logger";
import PageNotFoundException from "./PageNotFoundException";
import IFunction from "../types/IFunction";
import { IOneRequest } from "../types";
import OneRequest from "./OneRequest";
import OneContext from "./OneContext";
import HTTPError from "./HTTPError";
import HTTPStatus from "../utils/HTTPStatus";
import getContext from "../utils/getContext";
import MimeType from "./MimeType";

const app: Application = express();
const server = new Server(app);
const context: OneContext = getContext();

app.enable("trust proxy");
app.set("version", getVersion());

app.use(
  helmet({
    contentSecurityPolicy: false,
  })
);

app.use(
  bodyParser.json({
    limit: context.string("ONELESS_BODY_LIMIT", "5mb"),
  })
);
app.use(
  bodyParser.urlencoded({
    limit: context.string("ONELESS_BODY_LIMIT", "5mb"),
    extended: true,
  })
);
app.use(
  bodyParser.text({
    limit: context.string("ONELESS_BODY_LIMIT", "5mb"),
  })
);
app.use(cors({ origin: "*" }));
app.use((req: Request, res: Response, next: NextFunction) => {
  res.setHeader("X-Oneless-Version", `v${ getVersion() }`);
  next();
});

app.use(
  morgan("tiny", {
    stream: { write: (message) => AppLogger.verbose(message.trim()) },
  })
);

function errorLogger() {
  app.use((req: Request, res: Response, next: NextFunction) => {
    next(new PageNotFoundException());
  });

  app.use((error: any, req: Request, res: Response, next: NextFunction): void => {
    const status = error.status || HTTPStatus.SERVER_ERROR;
    const message = error.message;
    let { details } = error;

    if (details) {
      details = details.reduce(
        (accum, detail) => ({
          ...accum,
          [detail.path.join(".")]: `${ detail.message }${ detail.context && ` (Value: ${ detail.context.value })` }`,
        }),
        {}
      );
    } else {
      details = "";
    }

    AppLogger.error(`[HTTP ${ status }]: ${ message }`);
    AppLogger.silly(error);
    AppLogger.silly(error.stack);
    if (details) {
      AppLogger.silly(JSON.stringify(details));
    }

    res.status(status).json({
      status,
      message,
      details,
    });
    AppLogger.info(`POST ${ status } ${ req.path }`);
    AppLogger.verbose(
      JSON.stringify({
        status,
        message,
        details,
      })
    );
  });
}

process.on("uncaughtException", (err) => {
  AppLogger.error(err.message);
  AppLogger.debug(err.stack);
  process.exit(1); //mandatory (as per the Node.js docs)
});

process.on("SIGINT", () => {
  AppLogger.info(`Application closed.`);
  process.exit(1); //mandatory (as per the Node.js docs)
});

const obs = new PerformanceObserver((items) => {
  const duration = items.getEntries()[0].duration.toFixed(2);
  const memoryUsage = process.memoryUsage().heapUsed / 1024 / 1024;
  AppLogger.info(`Execution Time: ${ duration }ms. Used Memory: ${ Math.round(memoryUsage * 100) / 100 }MB`);
  performance.clearMarks();
});
obs.observe({ entryTypes: [ "measure" ] });

export { context };

export default async function OneServer(functionDefinitions: Record<string, IFunction>, port = 3000): Promise<void> {
  const appURL = `http://localhost:${ port }`;

  return new Promise((resolve, reject) => {
    if (Object.keys(functionDefinitions).length === 0) {
      reject(new HTTPError("There are no functions defined to be loaded"));
      return;
    }

    for (const functionDefinitionKey of Object.keys(functionDefinitions)) {
      const functionDefinition = functionDefinitions[functionDefinitionKey];
      AppLogger.info(`${ functionDefinition.method.toUpperCase() } ${ appURL }/${ functionDefinition.name }`);
      app[functionDefinition.method](`/${ functionDefinition.name }`, async (req: Request, res: Response, next: NextFunction) => {
        performance.mark(`Start running ${ functionDefinition.name }`);
        try {
          const request: IOneRequest = new OneRequest(req);
          const response = await functionDefinition.function(context, request);

          AppLogger.info(`${ functionDefinition.method.toUpperCase() } ${ response.status } /${ functionDefinition.name }`);
          AppLogger.silly(`Content-Type: ${ response.isFile ? "binary" : response.contentType || MimeType.PLAIN_TEXT }`);
          AppLogger.silly(`Body: ${ JSON.stringify(response.body) }`);

          if (response.status === HTTPStatus.MOVED_PERMANENTLY || response.status === HTTPStatus.REDIRECT) {
            res.status(response.status).redirect(response.body);
            performance.measure(`End running ${ functionDefinition.name }`, `Start running ${ functionDefinition.name }`);
            return;
          }
          if (response.isFile) {
            res.download(response.body, response.fileName);
            performance.measure(`End running ${ functionDefinition.name }`, `Start running ${ functionDefinition.name }`);
            return;
          }

          if (response.contentType) {
            res.contentType((response.contentType || MimeType.PLAIN_TEXT) as string);
            res.send(response.body as string);
            performance.measure(`End running ${ functionDefinition.name }`, `Start running ${ functionDefinition.name }`);
            return;
          }
          res.status(response.status).json(response.body);
          performance.measure(`End running ${ functionDefinition.name }`, `Start running ${ functionDefinition.name }`);
        } catch (err) {
          next(err);
        }
      });
    }

    errorLogger();

    server.listen(port, () => {
      AppLogger.info(`Application started: ${ appURL }`);
      resolve();
    });

    server.on("close", () => {
      AppLogger.info(`Application closed.`);
    });
  });
}
