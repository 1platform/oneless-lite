import winston, { Logger as WinstonLogger } from "winston";
import Transport from "winston-transport";
import LogLevel from "./LogLevel";
import { Format } from "logform";
import Env from "./Env";

export { default as LogLevel } from "./LogLevel";

/**
 * The base logger class to be used in the application.
 *
 * This class is the base of the available loggers in the application. In order
 * to use this class you need a transport and the level of information you
 * want to log.
 */
export abstract class Logger {
  /**
   * The constructor of the logger class.
   *
   * @param logLevel The level of logging we will use in our application.
   * @param transport The transport used for the winston logger.
   */
  public constructor(logLevel: LogLevel, transport: Transport) {
    this._log = winston.createLogger({
      level: logLevel.toString() || LogLevel.INFO,
      format: winston.format.simple(),
      transports: [ transport ],
    });
  }

  /**
   * The logger object the application will use for logging.
   */
  protected _log: WinstonLogger;

  /**
   * Getter for the logger object.
   */
  public get log(): WinstonLogger {
    return this._log;
  }

  /**
   * Method to log an information message.
   *
   * @param message The message to be logged.
   * @param meta Various other meta elements passed to the log method.
   */
  public info(message: any, ...meta: any[]): WinstonLogger {
    return this.log.info(message, ...meta);
  }

  /**
   * Method to log an error message.
   *
   * @param message The message to be logged.
   * @param meta Various other meta elements passed to the log method.
   */
  public error(message: any, ...meta: any[]): WinstonLogger {
    return this.log.error(message, ...meta);
  }

  /**
   * Method to log a warning message.
   *
   * @param message The message to be logged.
   * @param meta Various other meta elements passed to the log method.
   */
  public warn(message: any, ...meta: any[]): WinstonLogger {
    return this.log.warn(message, ...meta);
  }

  /**
   * Method to log a debug message.
   *
   * @param message The message to be logged.
   * @param meta Various other meta elements passed to the log method.
   */
  public debug(message: any, ...meta: any[]): WinstonLogger {
    return this.log.debug(message, ...meta);
  }

  /**
   * Method to log a verbose message.
   *
   * @param message The message to be logged.
   * @param meta Various other meta elements passed to the log method.
   */
  public verbose(message: any, ...meta: any[]): WinstonLogger {
    return this.log.verbose(message, ...meta);
  }

  /**
   * Method to log a silly message.
   *
   * @param message The message to be logged.
   * @param meta Various other meta elements passed to the log method.
   */
  public silly(message: any, ...meta: any[]): WinstonLogger {
    return this.log.silly(message, ...meta);
  }
}

/**
 * The console logger that can be used in your application.
 */
export class ConsoleLogger extends Logger {
  /**
   * The constructor of the logger class.
   *
   * @param logLevel The level of logging we will use in our application.
   * @param format The message formatter.
   */
  public constructor(logLevel: LogLevel, format?: Format) {
    super(
      logLevel,
      new winston.transports.Console({
        format,
      })
    );
  }
}

/**
 * The default logger of the application.
 */
const DefaultLogger: Logger = new ConsoleLogger(LogLevel[Env.string("LOG_LEVEL", LogLevel.INFO).toUpperCase()]);

export default DefaultLogger;
