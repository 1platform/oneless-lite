import { IOneResponse } from "../types";
import HTTPStatus from "./HTTPStatus";
import IOneErrorResponse from "../types/IOneErrorResponse";
import IOneMessageResponse from "../types/IOneMessageResponse";

export function OKResponse<T = any>(body: T, isFile = false): IOneResponse<T> {
  return {
    status: HTTPStatus.OK,
    body,
    isFile,
  };
}

export function OKMessageResponse<T = any>(message?: string): IOneResponse<IOneMessageResponse> {
  return {
    status: HTTPStatus.OK,
    body: {
      status: "ok",
      code: HTTPStatus.OK,
      message: message || "OK!",
    },
  };
}

export function OKFileResponse(body: string): IOneResponse<string> {
  return {
    status: HTTPStatus.OK,
    body,
    isFile: true,
  };
}

export function NotFoundResponse(): IOneResponse<string> {
  return {
    status: HTTPStatus.NOT_FOUND,
    body: "not-found",
  };
}

export function NoContentResponse(): IOneResponse<void> {
  return {
    status: HTTPStatus.NO_CONTENT,
    body: null,
  };
}

export function AcceptedResponse(): IOneResponse<void> {
  return {
    status: HTTPStatus.ACCEPTED,
    body: null,
  };
}

export function CreatedResponse<T = any>(body: T): IOneResponse<T> {
  return {
    status: HTTPStatus.CREATED,
    body,
  };
}

export function RedirectResponse(url: string): IOneResponse<string> {
  return {
    status: HTTPStatus.REDIRECT,
    body: url,
  };
}

export function MovedResponse(url: string): IOneResponse<string> {
  return {
    status: HTTPStatus.MOVED_PERMANENTLY,
    body: url,
  };
}

export function ForbiddenResponse(message?: string): IOneResponse<IOneErrorResponse> {
  return {
    status: HTTPStatus.FORBIDDEN,
    body: {
      status: "error",
      code: HTTPStatus.FORBIDDEN,
      message: message || "You are not allowed!",
    },
  };
}

export function UnauthorizedResponse(message?: string): IOneResponse<IOneErrorResponse> {
  return {
    status: HTTPStatus.UNAUTHORIZED,
    body: {
      status: "error",
      code: HTTPStatus.UNAUTHORIZED,
      message: message || "You are not authorized to perform this action!",
    },
  };
}

export function ServerErrorResponse(message?: string): IOneResponse<IOneErrorResponse> {
  return {
    status: HTTPStatus.SERVER_ERROR,
    body: {
      status: "error",
      code: HTTPStatus.SERVER_ERROR,
      message: message || "There was a server error while performing the desired action!",
    },
  };
}

export function BadRequestResponse(message?: string): IOneResponse<IOneErrorResponse> {
  return {
    status: HTTPStatus.BAD_REQUEST,
    body: {
      status: "error",
      code: HTTPStatus.BAD_REQUEST,
      message: message || "The data passed to this request is bad... Please fix it!",
    },
  };
}
