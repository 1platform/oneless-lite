import OneContext from "../engine/OneContext";

let context: OneContext;

export default function getContext() {
  if (!context) {
    context = new OneContext();
  }

  return context;
}

export function reloadContext(): OneContext {
  context.reload();

  return context;
}
