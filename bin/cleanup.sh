#!/bin/bash

echo 'Cleanup Docker Images'
docker builder prune -f
docker image prune -f
docker container prune -f
docker volume prune -f

